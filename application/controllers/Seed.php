<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Seed extends CI_Controller
{

  public function index()
	{
    $data['result'] = $this->api_model->seed();
    $this->load->view('output',$data);
  }

}
