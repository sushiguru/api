<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends CI_Controller
{

  public function __construct()
  {
    header('Access-Control-Allow-Origin: *');
    header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
    header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE");
    $method = $_SERVER['REQUEST_METHOD'];
    if($method === "OPTIONS")
    {
      die();
    }
    parent::__construct();
  }

  public function index()
	{
	}


	public function get()
  {
    $product_id = $this->uri->segment(3, 0);
    $data['result'] = $this->api_model->get($product_id);
    $this->load->view('output',$data);
  }

  public function post()
  {
    // POST inserts a new product
    $post = $_GET;
    // mock data - replace with $_GET data in production
    /*$post = array(
      'name' => 'New product name',
      'price' => '999',
      'description' => 'This is a new product',
      'category_id' => '3'
    );*/

    $data['result'] = $this->api_model->post($post);
    $this->load->view('output',$data);
  }

  public function put()
  {
    // PUT should update an existing product
    $post = $_GET;
    // mock data - replace with $_GET data in production
    /*$post = array(
      'id' => 1,
      'name'=>'LG P880 4X HD',
      'description' => 'Not such an awesome phone now',
      'price' => '10'
    );*/
    $data['result'] = $this->api_model->put($post);
    $this->load->view('output',$data);
  }

  public function delete()
  {
    $post = $_GET;
    // mock data - replace with $_POST data in production
    /*$post = array(
      'id' => 1,
    );*/

    $data['result'] = $this->api_model->delete($post);
    $this->load->view('output',$data);
  }

}
