<?php

class Api_model extends CI_Model
{

  public function __construct()
  {
    parent::__construct();
  }

  public function get($product_id)
  {
    // query products for all or individual by ID
    $this->db->select('c.name as category_name, p.id, p.name, p.description, p.price, p.category_id, p.created');
    $this->db->from('products p');
    $this->db->join('categories c', 'p.category_id = c.id');
    $this->db->order_by('p.id','ASC');
    if ($product_id != 0)
    {
      $this->db->where('p.id', $product_id);
    }

    $query = $this->db->get();

    $numrows = $query->num_rows();

    // check if more than 0 record found
    if ($numrows > 0)
    {

      // products array
      $products_arr = array();
      $products_arr['products'] = array();

      // retrieve our table contents
      foreach ($query->result_array() as $row)
      {
        // extract row
        // this will make $row['name'] to
        // just $name only
        extract($row);

        $product_item = array(
          'id' => $id,
          'name' => $name,
          'description' => html_entity_decode($description),
          'price' => $price,
          'category_id' => $category_id,
          'category_name' => $category_name
        );
        if($numrows==1)
        {
          $products_arr = $product_item;
        }
        else
        {
          array_push($products_arr['products'], $product_item);
        }
      }

      $json = json_encode($products_arr);
    }
    else
    {
      $json = json_encode(
        array('message' => 'No products found.')
      );
    }

    return $json;
  }

  public function post($post)
  {
    // query to insert record
    $data = array(
      'name' => $post['name'],
      'price' => $post['price'],
      'description' => $post['description'],
      'category_id' => $post['category_id'],
      'created' => date('Y-m-d H:i:s')
    );

    if ($this->db->insert('products', $data))
    {
      $product_id = $this->db->insert_id();
      $json = json_encode(array(
        'message'=>'success',
        'url'=>base_url().'product/get/'.$product_id
      ));
    }
    else
    {
      $json = json_encode(array(
        'message'=>'error:' . $this->db->error()['code']
      ));
    }
    return $json;
  }

  public function put($post)
  {
    // query to update record
    $id = $post['id'];
    unset($post['id']);
    $post['modified'] = date('Y-m-d H:i:s');
    $this->db->where('id', $id);
    $this->db->update('products', $post);

    if($this->db->affected_rows() == 1)
    {
      $json = json_encode(array(
        'message'=>'success'
      ));
    }
    else
    {
      $json = json_encode(array(
        'message'=>'error:' .  $this->db->error()['code']
      ));
    }
    return $json;
  }

  public function delete($post)
  {
    // query to delete specific record
    $id = $post['id'];
    $this->db->where('id', $id);
    $this->db->delete('products');
    if($this->db->affected_rows() == 1)
    {
      $json = json_encode(array(
        'message'=>'success'
      ));
    }
    else
    {
      $json = json_encode(array(
        'message'=>'error:' . $this->db->error()['code']
      ));
    }
    return $json;
  }

  public function seed()
  {
    //read contents of seed.sql script file and execute to seed database
    $sql = file_get_contents(base_url().'seed.sql');
    $statements = explode(';', $sql);
    array_pop($statements);

    foreach($statements as $statement)
    {
      $statement = $statement . ';';
      $this->db->query($statement);
    }
    $json = json_encode(array(
        'message'=>'done'
      ));

    return $json;
  }

}